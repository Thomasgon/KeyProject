#include <LWiFi.h>          //on inclut toutes les bibliotèques nécessaires
#include <rgb_lcd.h>
#include <LBattery.h>
#include <Wire.h>
//include de la partie Bluetooth
#include <LBT.h>
#include <LBTServer.h>
#define SPP_SVR "StarPla" //On défini le nom du serveur Bluetooth

#include <LWiFiClient.h>

LWiFiClient c;      //objet qui va nous permettre de nous connecter



rgb_lcd lcd;                //on crée un objet rgb_lcd pour pouvoir contrôler le LCD
char buff[256];
int nivBat;                 //niveau de la batterie
int charge;                 //pour savoir si on est en train de charger la batterie ou non
const int colorR = 255;     //pour définir le code couleur
const int colorG = 255;
const int colorB = 255;
int read_size = 0;
void setup() {
  
  Serial.begin(9600);           //on démarre la communication série
  pinMode(2, OUTPUT);
  pinMode(13, OUTPUT);
  //Partie Batterie
  pinMode(13, OUTPUT);
 
  //On définit le nombre de colonnes et de lignes que l'on aura pour le LCD
  lcd.begin(16, 2);
    
  lcd.setRGB(colorR, colorG, colorB);
    
  // On va afficher un message sur l'écran LCD
  lcd.print ("Battery: ");
  lcd.print(LBattery.level());  //on affiche le niveau de la batterie
  lcd.print ("%");
    
    
  
  //Partie Bluetooth
  
  Serial.printf("start BTS\n");       //on annonce que l'on va lancer le serveur
  
  
}
void loop() {  
  
 //Partie Wi-Fi//
  LWiFi.begin();                        // On active le wifi
  LWiFi.connect("eduspot");                 //on se connecte à eduspot
  delay(10000);                       //on attend 10 secondes le temps qu'on soit bien connecté
  printCurrentNet();                    //on affiche les informations de l'AP sur lequel on est connecté
  

  
 //Partie Batterie//
  sprintf(buff,"battery level = %d", LBattery.level() ); 
  Serial.println(buff);
  nivBat=LBattery.level();
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  
  if (nivBat<100) {                     //si le niveau de la batterie est inférieur à 100% on fait clignoter la LED et on fait buzzer le buzzeur
   lcd.setRGB(255, 0, 0);               //on change la couleur de l'écran
   signaler();
  } 
 delay(1000); 
 
 
 //Partie Bluetooth
 
 bool success = LBTServer.begin((uint8_t*)SPP_SVR); //pour vérifier si le Bluetooth a bien été lancé
  if( !success )
  {
      Serial.printf("Cannot begin Bluetooth Server successfully\n");
  }
  else
  {
      Serial.printf("Bluetooth Server begin successfully\n"); //on annonce que le serveur a bien été lancé
  }
 
  // On attend pour voir si on a un client qui se connecte
  bool connected = LBTServer.accept(20); //on met un timeout de 20secondes, le temps qu'a l'utilisateur pour se connecter avant le prochain cycle
 int sent =0;
  if( !connected )
  {
      Serial.printf("No connection request yet\n");
      // si aucun périphérique n'est connecté
      sent=1; //on met sent à 1 pour dire que l'on ne va pas à recevoir de données
  }
  else
  {
      Serial.printf("Connected\n"); //si un périphérique est connecté on le notifie
  }
  
  if (!sent)
  {
      char buffer[32] = {0};
      
      memset(buffer, 0, sizeof(buffer));
        delay (10000); //on attends 10 secondes et si l'utilisateur a rien tapé, on arrête la communication
        if(LBTServer.available())
        {
          read_size = LBTServer.readBytes((uint8_t*)buffer, 32); //on va lire ce qu'envoie le téléphone
        }
      
      Serial.printf("%s[%d]\n",  buffer, read_size);//on affiche ce qui est envoyé par le téléphone sur le moniteur série
     
      if(c.connect("10.42.246.14", 8888)){  //si on se connecte au serveur, on envoie les données
       c.println(buffer);

     }
  }
  LBTServer.end(); //on arrête le serveur Bluetooth
  delay(10000);
  LWiFi.end();                           // on éteins le wifis

  
}
  
void printCurrentNet() {                  //fonction qui va nous permettre d'afiicher les détails de l'AP sur lequel on est connecté
  
  // on afficher le SSID de l'AP sur lequel on est connecté
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // On affiche l'adresse MAC de l'AP sur lequel on est connecté
  byte bssid[6];
  LWiFi.BSSID(bssid);    
  Serial.print("BSSID: ");
  
  for (int compteur =0; compteur<5; compteur++){        //on affiche "un a un" chaque partie de l'adresse MAC de l'AP 
    Serial.print(bssid[compteur],HEX);
    Serial.print(":");                    //on affiche sous la même forme que si on était sur un pc AA:AA:AA:AA:AA:AA
  }
  Serial.println(bssid[5],HEX);               //on affiche la dernière partie de l'adresse MAC avec un retour à la ligne
  long rssi = LWiFi.RSSI();                 //on stocke la valeur de la puissance du signal reçue de l'AP
  Serial.print("signal strength (RSSI):");          //on affiche la puissance du signal de l'AP
  Serial.println(rssi);
}
void signaler() {     //fonction pour faire clignoter la LED et activer le buzzer
  digitalWrite(13, HIGH);   //on allume la LED et le buzzer
  delay(1000);               //on attend 1s
  digitalWrite(13, LOW);    //on éteint la LED et le buzzer
  delay(1000);               //on attend une nouvelle fois 1s
}