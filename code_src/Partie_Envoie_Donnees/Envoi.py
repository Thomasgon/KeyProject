#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      fontaine
#
# Created:     28/02/2017
# Copyright:   (c) fontaine 2017
# Licence:     BSD
#-------------------------------------------------------------------------------

import socket       #on import socket pour pouvoir utiliser les socket

import time


socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.bind(('', 8888)) #le socket sera sur le port 8888

while True:
        socket.listen(8)
        client, address = socket.accept()
        #print "{} connected".format( address ) #on va affcicher l'adresse et le port sur lequel on est connect? avec la cle

        response = client.recv(255)
        if response != "":          #si on recoit une r?ponse, on l'affiche
                print response+";"+str(time.time()) #on rajoute un timestamp pour avoir la date de connexion


print "Close"
client.close()
stock.close()